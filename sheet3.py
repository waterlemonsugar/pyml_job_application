import numpy as np
from minified import max_allowed_loops, no_imports
from typing import Dict, List, Tuple, Optional
from utils import train_test_idxs
import matplotlib.pyplot as plt

@no_imports
@max_allowed_loops(1)
def read_from_file(file: str = "data/data.csv") -> Dict[str, List[Tuple[float, float]]]:
    """
    Opens a csv file and parses it line by line. Each line consists of a label and two
    data dimensions. The function returns a dictionary where each key is a label and
    the value is a list of all the datapoints that have that label. Each datapoint
    is represented by a pair (2-element tuple) of floats.

    Args:
        file (str, optional): The path to the file to open and parse. Defaults to
        "data.csv".

    Returns:
        Dict[str, List[Tuple[float, float]]]: The parsed contents of the csv file
    """
    # YOUR CODE HERE
    D = dict()

    with open(file) as f:
        for line in f:
            splitted_line = line.rstrip().replace("\n", "").split(",")
            label, point = splitted_line[0], (float(splitted_line[1]), float(splitted_line[2]))
            if label in D:
                D[label].append(point)
            else:
                D[label] = [point]
    # YOUR CODE HERE

    return D


@no_imports
@max_allowed_loops(1)
def stack_data(D: Dict[str, List[Tuple[float, float]]]) -> Tuple[np.ndarray, np.ndarray]:
    """
    Convert a dictionary dataset into a two arrays of data and labels. The dictionary
    keys represent the labels and the value mapped to each key is a list that
    contains all the datapoints belonging to that label. The output are two arrays
    the first is the datapoints in a single 2d array and a vector of intergers
    with the coresponding label for each datapoint. The order of the datapoints is
    preserved according to the order in the dictionary and the lists.

    The labels are converted from a string to a unique int.

    The datapoints are entered in the same order as the keys in the `D`. First
    all the datapoints of the first key are entered then the second and so on.
    Within one label order also remains.

    Args:
        D (Dict[str, List[Tuple[float, float]]]): The dictionary that should be stacked.

    Returns:
        Tuple[np.ndarray, np.ndarray]: The two output arrays. The first is a
        float-matrix containing all the datapoints. The second is an int-vector
        containing the labels for each datapoint.
    """

    # YOUR CODE HERE
    # TODO init empty without random values
    y = np.array([0], dtype=np.int64)
    X = np.array([[0,0]])

    numeric_label = 0
    for label in D:
        points = D[label]
        labels = np.full_like(a=np.empty(shape=(len(points))), fill_value=numeric_label, dtype=np.int64)

        X = np.vstack((X, points))
        y = np.hstack((y, labels))

        numeric_label += 1

    y = y[1:]
    X = X[1:]

    # YOUR CODE HERE

    return X, y


@no_imports
@max_allowed_loops(1)
def get_clusters(X: np.ndarray, y: np.ndarray) -> List[np.ndarray]:
    """
    Receives a labeled dataset and splits the datapoints according to label

    Args:
        X (np.ndarray): The dataset
        y (np.ndarray): The label for each point in the dataset

    Returns:
        List[np.ndarray]: A list of arrays where the elements of each array
        are datapoints belonging to the label at that index.

    Example:
    >>> get_clusters(
            np.array([[0.8, 0.7], [0, 0.4], [0.3, 0.1]]),
            np.array([0,1,0])
        )
    >>> [array([[0.8, 0.7],[0.3, 0.1]]),
         array([[0. , 0.4]])]
    """
    # YOUR CODE HERE
    bins = []
    labels = np.unique(y)
    for label in labels:
        # TODO ask why argwhere returns list of lists [[0] [2]] instead of [0 2]
        ix = np.argwhere(y == label)
        ix = ix.reshape((len(ix)))
        binnie = X[ix]
        bins.append(binnie)
    # YOUR CODE HERE
    return bins


@no_imports
@max_allowed_loops(0)
def split(X: np.ndarray, y: np.ndarray) -> Tuple[List[np.ndarray], List[np.ndarray]]:
    """
    Split the data into train and test sets. The training and test set are
    clustered by label using `get_clusters`. The size of the training set
    is 80% of the whole dataset

    Args:
        X (np.ndarray): The dataset (2d)
        y (np.ndarray): The label of each datapoint in the dataset `X` (1d)

    Returns:
        Tuple[List[np.ndarray], List[np.ndarray]]: The clustered training and
        testset
    """
    # YOUR CODE HERE
    tr_ixs, te_ixs = train_test_idxs(len(y), 0.2)
    tr_clusters = get_clusters(X[tr_ixs], y[tr_ixs])
    te_clusters = get_clusters(X[te_ixs], y[te_ixs])
    # YOUR CODE HERE

    return tr_clusters, te_clusters

@no_imports
@max_allowed_loops(1)
def calc_means(clusters: List[np.ndarray]) -> np.ndarray:
    """
    For a collections of clusters calculate the mean for each cluster

    Args:
        clusters (List[np.ndarray]): A list of 2d arrays

    Returns:
        np.ndarray: A matrix where each row represents a mean of a cluster

    Example:
        >>> tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        >>> calc_means(tiny_clusters)
        array([[0.15, 0.25]), [0.7,0.7]])
    """
    # YOUR CODE HERE
    means = []
    for cluster in clusters:
        means.append(np.mean(cluster, axis=0))

    return np.array(means)
    # YOUR CODE HERE


"""
Scatter plot of clusters 



+ Create a scatter plot of size 8x8.

Plot each datapoint of a cluster  𝑥𝑖𝑘∈𝐶𝑘  as dots with an alpha value of 0.6 and a label.

+ The plot-label should contain both the cluster's letter-label as well as its integer-label.

+ Further, plot the cluster's mean  𝜇𝑘  as a red cross of size 7. The plot should also have a label 
for each cluster's mean, giving information on its exact coordinates.

+ The title of the plot should be 'Scatter plot of the clusters' in fontsize 20.

+ Label for the scatter plots example: A = 0

+ Label for the cluster means example (use LaTeX):  𝜇𝐴: [1.23 0.56]

+ If the mean of each cluster is not provided, use calc_means(clusters) to calculate the means.

+ Number of loops allowed in this exercise: 1 (for iteration over the clusters)

"""
@no_imports
def plot_scatter_and_mean(
        clusters: List[np.ndarray],
        letters: List[str],
        means: Optional[List[np.ndarray]] = None,
) -> None:
    """
    Create a scatter plot visulizing each cluster and its mean

    Args:
        clusters (List[np.ndarray]): A list containing arrrays representing
        each cluster
        letters (List[str]): The "name" of each cluster
        means (Optional[List[np.ndarray]]): The mean of each cluster. If not
        provided the mean of each cluster in `clusters` should be calculated and
        used

    """
    assert len(letters) == len(clusters)

    # YOUR CODE HERE
    if means is None:
        means = calc_means(clusters)

    title = "Scatter plot of the clusters"
    plt.figure(figsize=(8, 8))
    integer_label = 0
    for cluster in clusters:
        label = letters[integer_label]
        plt.scatter(*cluster.T, s=20, alpha=0.6, label=f"{label} = {integer_label}")
        mean = means[integer_label]
        plt.plot(*mean, 'X', ms=7, label=f'$\\mu {label} =$'+f'[{mean[0]:0.2f} {mean[1]:0.2f}]')
        integer_label += 1
    plt.title(title, fontsize=20)
    plt.legend(loc='best')

    # YOUR CODE HERE
    plt.show()


@no_imports
@max_allowed_loops(1)
def within_cluster_cov(clusters: List[np.ndarray]) -> np.ndarray:
    """
    Calculate the within class covariance for a collection of clusters

    Args:
        clusters (List[np.ndarray]): A list of clusters each consisting of
        an array of datapoints

    Returns:
        np.ndarray: The within cluster covariance

    Example:
        >>> within_cluster_cov(
            [array([[0.2, 0.3],
                    [0.1, 0.2]]),

            array([[0.8, 0.9],
                   [0.7, 0.5],
                   [0.6, 0.7]])]
        )
        >>> array([[0.025, 0.025],
                   [0.025, 0.085]])
    """
    d = clusters[0].shape[1]
    S_w = np.zeros((d, d))
    # YOUR CODE HERE
    for cluster in clusters:
        mean = np.mean(cluster, axis=0)
        S_k = (cluster-mean).T.dot(cluster-mean)
        S_w += S_k

    return S_w
    # YOUR CODE HERE


"""
To make it easier to visually analyse the the differences between clusters, 
the data can be projected onto an axis. 
Plot a histrogram for the projection onto the given axis.

+ The histogram should have 30 bins, be 50% transparent and labeled. 
The area under the histogram should be normalized and sum to 1 to represent a proper distribution. 
It can be done by setting the corresponding parameter. 

+ The bars width should have 4/5 of the bins width.

+ Create a scatter plot of size 14x5.
+ Plot the mean of each cluster as a vertical, dashed, red line.
+ Label for the histograms example: A
+ The title of the plot should be dynamic, have a font size of 20 and explain the axis of the projection, e.g. "Projection to axis 0 histogramm plot" or "Projection to axis 1 histogramm plot", depending on the axis.
+ Number of loops allowed in this exercise: 1 (to iterate over the clusters)

"""
@no_imports
def plot_projection(
        clusters: List[np.ndarray], letters: List[str], means: np.ndarray, axis: int = 0
):
    """
    Plot a histogram of the dimension provided in `axis`

    Args:
        clusters (List[np.ndarray]): The clusters from which to create the historgram
        letters (List[str]): The string representation of each class
        means (np.ndarray): The mean of each class
        axis (int): The axis from which to create the historgram. Defaults to 0.
    """
    # YOUR CODE HERE

    plt.figure(figsize=(14, 5))

    for i in range(len(clusters)):
        letter = letters[i]
        cluster = clusters[i]
        mean = means[i]
        print(cluster.shape)
        indices = cluster.shape[axis]
        projection = cluster[:, axis]
        plt.hist(projection, bins=30, rwidth=0.8, alpha=0.5, label=letter, density=True)
        plt.axvline(mean[axis], ls='--', c='r')

    title = f'Projection to axis {axis} histogramm plot'
    plt.title(title, fontsize=20)
    plt.legend(loc='best')

    # YOUR CODE HERE
    plt.show()


@no_imports
@max_allowed_loops(0)
def calc_mean_of_means(clusters: List[np.ndarray]) -> np.ndarray:
    """
    Given a collection of datapoints divided in clusters, calculate the
    mean of all cluster means.
    Args:
        clusters (List[np.ndarray]): A list of clusters represented in arrays

    Returns:
        np.ndarray: A single datapoint that represents the mean of all the
        cluster means

    Example:
        >>> calc_mean_of_means(
                [np.array([[0.222, 0.333], [0.1, 0.2]]), np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]])]
            )
        >>> array([0.4305 , 0.48325])
    """
    # YOUR CODE HERE
    return np.mean(calc_means(clusters), axis=0)
    # YOUR CODE HERE


@no_imports
@max_allowed_loops(1)
def between_cluster_cov(
        clusters: List[np.ndarray],
        cluster_means: List[np.ndarray],
        mean_of_means: np.ndarray,
) -> np.ndarray:
    """
    Calculate the covariance between clusters.

    Args:
        clusters (List[np.ndarray]): A list of datapoints divided by cluster
        cluster_means (List[np.ndarray]): A list of vectors representing the mean
        of each cluster
        mean_of_means (np.ndarray): A vector, the mean of all datapoints

    Returns:
        np.ndarray: Covariance between clusters

    Example:
        >>> tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        >>> tiny_means = [np.array([0.15, 0.25]), np.array([0.7, 0.7])]
        >>> tiny_mean_of_means = np.array([0.425, 0.475])
        >>> between_cluster_cov(tiny_clusters, tiny_means, tiny_mean_of_means)
        array([[0.378125, 0.309375],
               [0.309375, 0.253125]])

    """
    d = clusters[0].shape[1]
    S_b = np.zeros((d, d))
    # YOUR CODE HERE
    for i in range(len(clusters)):
        cluster = clusters[i]
        mean = cluster_means[i]
        diff = (mean - mean_of_means)[None]
        S_b += len(cluster)*diff*diff.T
        print(S_b)

    return S_b
    # YOUR CODE HERE


@no_imports
@max_allowed_loops(0)
def rotation_matrix(S_w: np.ndarray, S_b: np.ndarray) -> Tuple[np.ndarray, int]:
    """
    Calculate the transformation matrix given the within- and between cluster
    covariance matrices.

    Args:
        S_w (np.ndarray): The within cluster covariance
        S_b (np.ndarray): The between cluster covariance

    Returns:
        np.ndarray: The transformation matrix
        int: The axis along with the transformed data achieves maximal variance

    Example:
        >>> tiny_S_w = np.array([[0.025, 0.025], [0.025, 0.085]])
        >>> tiny_S_b = np.array([[0.378125, 0.309375], [0.309375, 0.253125]])
        >>> rotation_matrix(tiny_S_w, tiny_S_b)
        (array([[ 0.99752952, -0.63323779],
                [-0.07024856,  0.7739573 ]]), 0)
    """
    # YOUR CODE HERE
    A = np.linalg.inv(S_w).dot(S_b)
    eigvals, W = np.linalg.eig(A)
    axis = np.argmax(eigvals)
    return W, axis
    # YOUR CODE HERE


@no_imports
@max_allowed_loops(1)
def rotate_clusters(W_rot: np.ndarray, clusters: List[np.ndarray]) -> List[np.ndarray]:
    """
    Rotate all the datapoints in all the clusters

    Args:
        W_rot (np.ndarray): The rotation matrix
        clusters (List[np.ndarray]): The list of datapoints divided in clusters that
        will be rotated

    Returns:
        List[np.ndarray]: The rotated datapoints divided by cluster
    """
    # YOUR CODE HERE
    # this works in tests but does not correspond to formula
    rotated = []
    for c in clusters:
        r = c.dot(W_rot)
        rotated.append(r)

    return rotated
    # YOUR CODE HERE

