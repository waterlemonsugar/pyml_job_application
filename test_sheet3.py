import unittest
import numpy as np
from typing import Dict, List, Tuple, Optional
from utils import train_test_idxs
import sheet3 as s


class MyTestCase(unittest.TestCase):

    def test_read_from_file(self):
        tiny_result = s.read_from_file(file="data/tiny.csv")
        print("tiny_result", tiny_result)
        tiny_expected = {"A": [(0.8, 0.9), (0.2, 0.3)], "B": [(0.9, 0.1)], "C": [(2.0, 4.0)]}
        self.assertEqual(tiny_result, tiny_expected)

        D = s.read_from_file(file="data/data.csv")
        print(f"Keys of D: {D.keys()}", end="\n\n")
        for k, v in D.items():
            print(f"{len(v)} datapoints were assigned the label {k}")

        # Test All types
        self.assertIsInstance(D, dict)
        for d in D:
            self.assertIsInstance(d, str)
            self.assertIsInstance(D[d], list)
            for el in D[d]:
                self.assertIsInstance(el, tuple)
                self.assertIsInstance(el[0], float)
                self.assertIsInstance(el[1], float)

        letters = "MNU"
        self.assertEqual(set(D.keys()), set(letters))
        self.assertTrue(all(len(v) > 99 for v in D.values()))
        s.read_from_file.assert_not_too_many_loops()
        s.read_from_file.assert_no_imports()

    def test_stack_data(self):
        D = s.read_from_file(file="data/data.csv")

        tiny_expected_X, tiny_expected_y = (
            np.array(
                [
                    [0.0, 0.1],
                    [0.9, 0.7],
                    [0.8, 0.3],
                ]
            ),
            np.array([0, 1, 1]),
        )
        tiny_result_X, tiny_result_y = s.stack_data(
            {"B": [(0.0, 0.1)], "A": [(0.9, 0.7), (0.8, 0.3)]}
        )
        print(tiny_result_X, tiny_result_y)
        np.testing.assert_allclose(tiny_expected_X, tiny_result_X)
        np.testing.assert_allclose(tiny_expected_y, tiny_result_y)

        X, y = s.stack_data(D)
        print(X.shape, y.shape)
        print(X.dtype, y.dtype)

        expected_len = sum(len(x) for x in D.values())
        print(f"Expected length for X, y: {expected_len}")

        self.assertEqual(X.shape, (expected_len, 2))
        self.assertEqual(y.shape, (expected_len,))

        self.assertEqual(X.dtype, np.float64)
        self.assertEqual(y.dtype, np.int64)

        self.assertEqual(set(y), set(range(len(D))))

    def test_get_clusters(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        tiny_result = s.get_clusters(
            np.array(
                [
                    [0.8, 0.7],
                    [0, 0.4],
                    [0.3, 0.1],
                ]
            ),
            np.array([0, 1, 0]),
        )
        print("returned: ", tiny_result)
        tiny_expected = [
            np.array(
                [
                    [0.8, 0.7],
                    [0.3, 0.1],
                ]
            ),
            np.array(
                [
                    [0.0, 0.4],
                ]
            ),
        ]
        print("expected: ", tiny_expected)
        for r, e in zip(tiny_result, tiny_expected):
            np.testing.assert_allclose(r, e)

        clusters = s.get_clusters(X, y)
        # output is list
        self.assertIsInstance(clusters, List)
        self.assertEqual(len(letters), len(clusters))

        # all elements are arrays
        for el in clusters:
            self.assertIsInstance(el, np.ndarray)

        self.assertEqual(sum(map(len, clusters)), len(X))

    def test_split(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        output = s.split(X, y)
        tr_clusters, te_clusters = output
        self.assertIsInstance(output, Tuple)
        self.assertIsInstance(tr_clusters, List)
        self.assertIsInstance(te_clusters, List)

        self.assertEqual(len(tr_clusters), len(te_clusters))
        self.assertEqual(len(tr_clusters), len(letters))
        self.assertEqual(len(te_clusters), len(letters))

        for el in tr_clusters + te_clusters:
            self.assertIsInstance(el, np.ndarray)

        n_in_train = sum(map(len, tr_clusters))
        n_in_test = sum(map(len, te_clusters))
        self.assertEqual(n_in_train + n_in_test, len(X))

        percent_train = n_in_train / len(X)
        print(f"percent_train = {percent_train}")
        self.assertGreaterEqual(percent_train, 0.79)
        self.assertLessEqual(percent_train, 0.81)

    def test_calc_means(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        output = s.split(X, y)
        tr_clusters, te_clusters = output

        tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        tiny_result = s.calc_means(tiny_clusters)
        print(tiny_result, end="\n\n")
        tiny_expected = np.array([[0.15, 0.25], [0.7, 0.7]])
        np.testing.assert_allclose(tiny_result, tiny_expected)

        means = s.calc_means(tr_clusters)
        print(means)
        self.assertIsInstance(means, np.ndarray)
        self.assertEqual(means.shape, (len(letters), 2))

    def test_plot_scatter_and_mean(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        output = s.split(X, y)
        tr_clusters, _ = output

        s.plot_scatter_and_mean(tr_clusters, letters, means=None)

    def test_within_cluster_cov(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        output = s.split(X, y)
        tr_clusters, _ = output

        tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        tiny_expected = np.array([[0.025, 0.025], [0.025, 0.085]])
        tiny_result = s.within_cluster_cov(tiny_clusters)
        print(tiny_result)
        np.testing.assert_allclose(tiny_expected, tiny_result)

        S_w = s.within_cluster_cov(tr_clusters)
        print(S_w)
        self.assertIsInstance(S_w, np.ndarray)
        self.assertEqual(S_w.shape, (2, 2))

        # check if symmetric
        np.testing.assert_allclose(S_w, S_w.T)

    def test_plot_projection(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)
        letters = "MNU"

        output = s.split(X, y)
        tr_clusters, _ = output
        means = s.calc_means(tr_clusters)

        s.plot_projection(tr_clusters, letters, means, axis=1)

    def test_indices(self):
        a = np.arange(12)
        a = a.reshape(4,3)
        print(a)
        print(a[:, 0])

    def test_calc_mean_of_means(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)

        output = s.split(X, y)
        tr_clusters, _ = output

        tiny_result = s.calc_mean_of_means(
            [
                np.array([[0.222, 0.333], [0.1, 0.2]]),
                np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
            ]
        )
        print(tiny_result)
        tiny_expected = np.array([0.4305, 0.48325])
        np.testing.assert_allclose(tiny_expected, tiny_result)

        mean_of_means = s.calc_mean_of_means(tr_clusters)
        print(mean_of_means)
        self.assertIsInstance(mean_of_means, np.ndarray)
        self.assertEqual(mean_of_means.shape, (2,))

    def test_between_cluster_cov(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)

        output = s.split(X, y)
        tr_clusters, _ = output

        means = s.calc_means(tr_clusters)
        mean_of_means = s.calc_mean_of_means(tr_clusters)

        tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        tiny_means = [np.array([0.15, 0.25]), np.array([0.7, 0.7])]
        tiny_mean_of_means = np.array([0.425, 0.475])
        S_b = s.between_cluster_cov(tiny_clusters, tiny_means, tiny_mean_of_means)
        S_b_exp = np.array([[0.378125, 0.309375], [0.309375, 0.253125]])
        np.testing.assert_allclose(S_b_exp, S_b)

        S_b = s.between_cluster_cov(tr_clusters, means, mean_of_means)
        print(S_b)
        self.assertIsInstance(S_b, np.ndarray)
        self.assertEqual(S_b.shape, (2, 2))
        np.testing.assert_allclose(S_b, S_b.T)

    def test_rotation_matrix(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)

        output = s.split(X, y)
        tr_clusters, _ = output

        means = s.calc_means(tr_clusters)
        mean_of_means = s.calc_mean_of_means(tr_clusters)

        S_w = s.within_cluster_cov(tr_clusters)
        S_b = s.between_cluster_cov(tr_clusters, means, mean_of_means)

        tiny_S_w = np.array([[0.025, 0.025], [0.025, 0.085]])
        tiny_S_b = np.array([[0.378125, 0.309375], [0.309375, 0.253125]])
        tiny_result_M, tiny_result_max_axis = s.rotation_matrix(tiny_S_w, tiny_S_b)
        print(tiny_result_M, tiny_result_max_axis)
        tiny_expected_M, tiny_expected_max_axis = (
            np.array([[0.99752952, -0.63323779], [-0.07024856, 0.7739573]]),
            0,
        )
        np.testing.assert_allclose(tiny_expected_M, tiny_result_M)
        np.testing.assert_allclose(tiny_expected_max_axis, tiny_result_max_axis)

        output = s.rotation_matrix(S_w, S_b)
        self.assertIsInstance(output, Tuple)
        self.assertEqual(len(output), 2)
        W_rot, max_axis = output
        self.assertIsInstance(W_rot, np.ndarray)
        self.assertIsInstance(max_axis, np.int64)
        self.assertEqual(W_rot.shape, (2, 2))

    def test_rotate_clusters(self):
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)

        output = s.split(X, y)
        tr_clusters, _ = output

        means = s.calc_means(tr_clusters)
        mean_of_means = s.calc_mean_of_means(tr_clusters)

        S_w = s.within_cluster_cov(tr_clusters)
        S_b = s.between_cluster_cov(tr_clusters, means, mean_of_means)
        output = s.rotation_matrix(S_w, S_b)
        W_rot, max_axis = output

        rad = np.deg2rad(30)
        c, s_ = np.cos(rad), np.sin(rad)
        rot30 = np.array([[c, -s_], [s_, c]])
        tiny_clusters = [
            np.array([[0.2, 0.3], [0.1, 0.2]]),
            np.array([[0.8, 0.9], [0.7, 0.5], [0.6, 0.7]]),
        ]
        tiny_rotated_result = s.rotate_clusters(rot30, tiny_clusters)
        print(tiny_rotated_result)
        tiny_rotated_expected = [
            np.array([[0.32320508, 0.15980762], [0.18660254, 0.12320508]]),
            np.array(
                [[1.14282032, 0.37942286], [0.85621778, 0.0830127], [0.86961524, 0.30621778]]
            ),
        ]
        for r, e in zip(tiny_rotated_result, tiny_rotated_expected):
            np.testing.assert_allclose(r, e)

        rot_tr_clusters = s.rotate_clusters(W_rot, tr_clusters)
        self.assertIsInstance(rot_tr_clusters, List)
        for norm, rotated in zip(tr_clusters, rot_tr_clusters):
            self.assertIsInstance(rotated, np.ndarray)
            self.assertEqual(norm.shape, rotated.shape)

    def test_goal(self):
        letters = "MNU"
        D = s.read_from_file(file="data/data.csv")
        X, y = s.stack_data(D)

        output = s.split(X, y)
        tr_clusters, _ = output

        means = s.calc_means(tr_clusters)
        mean_of_means = s.calc_mean_of_means(tr_clusters)

        S_w = s.within_cluster_cov(tr_clusters)
        S_b = s.between_cluster_cov(tr_clusters, means, mean_of_means)
        output = s.rotation_matrix(S_w, S_b)
        W_rot, max_axis = output

        rot_tr_clusters = s.rotate_clusters(W_rot, tr_clusters)

        s.plot_scatter_and_mean(rot_tr_clusters, letters)
        means = s.calc_means(rot_tr_clusters)
        s.plot_projection(rot_tr_clusters, letters, means, axis=max_axis)


if __name__ == '__main__':
    unittest.main()
